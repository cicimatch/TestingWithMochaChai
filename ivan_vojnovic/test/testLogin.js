const app = require('../app');

const supertest = require('supertest');
const chai = require('chai');
const expect = chai.expect;
const should = chai.should;

const api = supertest('http://localhost:3000');

let login = null;       

describe('*** Login with the failure ***', () => {

    // Try to put special character in the fields
	it('Login user with the special character', (done) => {
		api.post('/sign-in')
			.send({
				password: 'Password',
				email: 'Email',
			})
			.then((res) => {
                login = res.body.access_token;
                //console.log(res.body.message);
                expect(res.body.message).to.be.equal('Wrong password or email');
				done();
			}).
			catch(done);
		
    });

     /* Try to put one blank space in the field "password"
	it('Login with the character as blank space', (done) => {
		api.post('/sign-in')
			.send({
				password: ' password',
				email: 'email',
			})
			.then((res) => {
                login = res.body.access_token;
                console.log(res.body.message);
                expect(res.body.message).to.be.equal('Wrong password or email');
				done();
			}).
			catch(done);
		
    });

     // Try to put corect pass, and wrong email
	it('Login with the bad email name', (done) => {
		api.post('/sign-in')
			.send({
				password: 'password',
				email: 'emailZZZ',
			})
			.then((res) => {
                login = res.body.access_token;   // bug b2
                console.log(res.body.message);
                expect(res.body.message).to.be.equal('Wrong password or email');
				done();
			}).
			catch(done);
		
    });*/

});