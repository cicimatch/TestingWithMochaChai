const app = require('../app');

const supertest = require('supertest');
const chai = require('chai');
const expect = chai.expect;
const should = chai.should;

const api = supertest('http://localhost:3000');

let login = null;       

describe('*** User cannot ***', () => {

    // Try to put empty "email" field
	it('login with wrong email', (done) => {
		api.post('/sign-in')
			.send({
				password: 'password',
				email: ' ',
			})
			.then((res) => {
                login = res.body.access_token;
                console.log(login);      // bug b1
				expect(res.body.message).to.be.equal('Wrong password or email');
				//console.log(res.body.message);
				done();
			}).
			catch(done);
		
    });

    //Get Error massage for tokens
	it('get users with wrong token', (done) => {
        api.get('/users/:userId/accounts')
            .set('authorization', login + 1)
			.then((res) => {
                console.log(res.body.message);
                expect(res.body.message).to.be.equal('Missing authorization token');
				done();
			}).
		catch(done);
    });
    

});