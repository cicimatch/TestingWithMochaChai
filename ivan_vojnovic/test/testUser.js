const app = require('../app');

const supertest = require('supertest');
const chai = require('chai');
const expect = chai.expect;
const should = chai.should;

const api = supertest('http://localhost:3000');

let login = null;
let users = null;
describe('Tests', () => {

	// Login user to the system and fetch access token
	it('Login user', (done) => {
		api.post('/sign-in')
			.send({
				password: 'password',
				email: 'email',
			})
			.then((res) => {
				login = res.body.access_token;
				console.log(login);
				done();
			}).
			catch(done);
		
	});

	// Get a list of specific users
	it('Get user from list', (done) => {
		api.get('/users')
			.set('authorization', login)
			.then((res) => {
                // expected to got first user from app
                //console.log(res.body[0].name);
                expect(res.body[0].name).to.be.equal('User The One');
                expect(res.body[0].title).to.be.equal('Pljeskavica master');
				done();
			}).
		catch(done);
    });
    
    // 
    it('Get users from list', (done) => {
		api.get('/users')
			.set('authorization', login)
			.then((res) => {
                // play in console 
				console.log(res.body[5].name);
				console.log('----------');
				const prvi = res.body[1].active;
				console.log(prvi);
                expect(prvi).to.be.equal(true);
				done();
			}).
		catch(done);
    });

});